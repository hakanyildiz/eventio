# EVENTIO - HAKAN YILDIZ TEST PROJECT
> Eventio application built with Redux for state management, Webpack, Material-UI and React Router v4.

## HOW TO RUN?
```bash
# Install dependencies
npm install

# Run the development version
yarn start

# Run the production version
yarn run build
```

## APP INFO

I have used webpack for bundling. I have splitted the features into 3 webpack files.
When you check the 'package.json' file, you can see how to be used.

You can find the sources of project in the 'src' directory. 

* App.js which includes main router configurations is heart of our app.

I would like to explain what is the meaning of subfolders.
* `/pages` folder includes page react components. (As an old habit but I like seeing like that)

* `/components` folder includes all react components except page ones.
The application isn't big now so I didn't find necessary to follow the 'container' way.

* `/assets` folder includes all fonts, images and css files. 
App.css will be generated from App.scss file when the app runs.

* `/_helper` folder includes redux store and some configuration files.
I have stored API configurations in config.js file.
Auth-header returns an object has a token key.

* `/_actions` folder. Redux Actions
    - User.actions.js is used for requests of login and register.
    - Alert.actions.js is used for notification.
    - Event.actions.js is used for all event actions.
    We call related web service functions in the functions

* `/_reducers` folder. Redux Reducers

* `/_constans` folder includes contants which they help to communicate between Reducers and Actions.

* `/_services` folder includes files which are used HTTP functions.
According Web service API doc, I have created the functions for all events.