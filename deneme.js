class Tree {}

class Node extends Tree {
  constructor(children) {
    super()
    this.children = children
  }
}

class Leaf extends Tree {
  constructor(data) {
    super()
    this.data = data
  }
}

const foo = (t) => 
    t.constructor.name === 'Node' ? flatMap(t.children, foo)
  : t.constructor.name === 'Leaf' ? [t.data]
  : [];


let nodeExample = new Node("child");



let leafExample = new Leaf('hakke');
console.log('X', leafExample);

console.log('Foo x', foo(nodeExample))
