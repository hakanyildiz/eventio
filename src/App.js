import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { PrivateRoute } from './components/PrivateRoute';
import ToastContentWrapper from './components/ToastContentWrapper';

import Snackbar from '@material-ui/core/Snackbar';

import { history } from './_helpers';
import { alertActions } from './_actions';

import EventsPage from './pages/EventsPage';
import SignInPage from './pages/SignInPage';
import SignUpPage from './pages/SignUpPage';
import ResetPasswordPage from './pages/ResetPasswordPage';
import NoMatchPage from './pages/NoMatchPage';
import ProfilePage from './pages/ProfilePage';
import './assets/scss/App.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            toastStatus: true,
            toastId: 0,
        }

        const { dispatch } = this.props;

        history.listen((location, action) => {
            //clear alert on location change
            dispatch(alertActions.clear());
        });
    }
    componentDidUpdate(){
        const { toastId } = this.state;
        const { alert } = this.props;
        
        if ( toastId !== alert.id ) {
            this.setState({
                toastStatus: true,
                toastId: alert.id
            })
        }
    }

    handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        const { alert } = this.props;

        this.setState({ toastStatus: false, toastId: alert.id });
      };
    
    render(){
        const { alert } = this.props;
        const { toastStatus } = this.state;

        return (
                <Router history={history}>
                    <div id="app-container">
                        <div className="alert">
                            {alert && alert.type &&
                                <Snackbar
                                    anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                    }}
                                    open={toastStatus}
                                    // open={toastStatus}
                                    autoHideDuration={4000}
                                    onClose={this.handleCloseSnackbar}
                                >
                                    <ToastContentWrapper
                                        onClose={this.handleCloseSnackbar}
                                        variant={alert.type}
                                        message={alert.message}
                                    />
                                </Snackbar>
                            }
                        </div>
                        <Switch>
                            <PrivateRoute exact path="/" component={EventsPage} />
                            <PrivateRoute path='/events' component={EventsPage} />
                            <PrivateRoute path='/profile' component={ProfilePage} />
                            <Route path="/login" component={SignInPage} />
                            <Route path="/register" component={SignUpPage} />
                            <Route path="/reset-password" component={ResetPasswordPage} />
                            <Route component={NoMatchPage} />
                        </Switch>
                    </div>
                </Router>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

export default connect(mapStateToProps)(App);