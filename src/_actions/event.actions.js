import { eventContants } from '../_constants';
import { alertActions } from '.';
import { eventService, userService } from '../_services';
import { history } from '../_helpers';

export const eventActions = {
  getAllEvents,
  getEventView,
  getEventEdit,
  createEvent,
  updateEvent,
  deleteEvent,
  attendEvent,
  unattendEvent,
  resetEvents,
};

function resetEvents() {
  return { type: eventContants.EVENTS_RESET };
}

function attendEvent(id) {
  return dispatch => {
    dispatch(request());

    eventService.attendEvent(id).then(
      event => {
        dispatch(success(event));
        dispatch(alertActions.success('You attented the event!'));
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );

    // userService.tokenAuthentication().then(
    //     user => {
    //         const storedUser = JSON.parse(localStorage.getItem('user'));
    //         if (storedUser && user.id === storedUser.id) {
    //             eventService.attendEvent(id)
    //             .then(
    //                 event => {
    //                     dispatch(success(event));
    //                     dispatch(alertActions.success('You attented the event!'));
    //                 },
    //                 error => {
    //                     dispatch(failure(error.toString()));
    //                     dispatch(alertActions.error(error.toString()));
    //                 }
    //             );
    //         } else {
    //             userService.logout();
    //             history.push('/');
    //         }
    //     },
    //     error => {
    //         dispatch(failure(error.toString()));
    //         dispatch(alertActions.error(error.toString()));
    //     }
    // );
  };

  function request() {
    return { type: eventContants.ATTEND_EVENT_REQUEST };
  }
  function success(event) {
    return { type: eventContants.ATTEND_EVENT_SUCCESS, event };
  }
  function failure(id, error) {
    return { type: eventContants.ATTEND_EVENT_FAILURE, id, error };
  }
}

function unattendEvent(id) {
  return dispatch => {
    dispatch(request());
    eventService.unattendEvent(id).then(
      event => {
        dispatch(success(event));
        dispatch(alertActions.success('You unattented the event!'));
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );

    // userService.tokenAuthentication().then(
    //     user => {
    //         const storedUser = JSON.parse(localStorage.getItem('user'));
    //         if (storedUser && user.id === storedUser.id) {
    //             eventService.unattendEvent(id)
    //             .then(
    //                 response => {
    //                     dispatch(success(id));
    //                     dispatch(alertActions.success('You unattented the event!'));
    //                 },
    //                 error => {
    //                     dispatch(failure(error.toString()));
    //                     dispatch(alertActions.error(error.toString()));
    //                 }
    //             );
    //         } else {
    //             userService.logout();
    //             history.push('/');
    //         }
    //     },
    //     error => {
    //         dispatch(failure(error.toString()));
    //         dispatch(alertActions.error(error.toString()));
    //     }
    // );
  };

  function request() {
    return { type: eventContants.UNATTEND_EVENT_REQUEST };
  }
  function success(event) {
    return { type: eventContants.UNATTEND_EVENT_SUCCESS, event };
  }
  function failure(error) {
    return { type: eventContants.UNATTEND_EVENT_FAILURE, error };
  }
}

function getAllEvents() {
  return dispatch => {
    dispatch(request());

    eventService.getAllEvents().then(
      events => dispatch(success(events)),
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );
  };
  function request() {
    return { type: eventContants.GET_ALLEVENTS_REQUEST };
  }
  function success(events) {
    return { type: eventContants.GET_ALLEVENTS_SUCCESS, events };
  }
  function failure(error) {
    return { type: eventContants.GET_ALLEVENTS_FAILURE, error };
  }
}

function getEventView(id) {
  return dispatch => {
    dispatch(request(id));

    eventService.getEventById(id).then(
      event => dispatch(success(event)),
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );
  };

  function request(id) {
    return { type: eventContants.GET_EVENTVIEW_REQUEST, id };
  }
  function success(event) {
    return { type: eventContants.GET_EVENTVIEW_SUCCESS, event };
  }
  function failure(id, error) {
    return { type: eventContants.GET_EVENTVIEW_FAILURE, id, error };
  }
}

function getEventEdit(id) {
  return dispatch => {
    dispatch(request(id));

    eventService.getEventById(id).then(
      event => dispatch(success(event)),
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );
  };

  function request(id) {
    return { type: eventContants.GET_EVENTEDIT_REQUEST, id };
  }
  function success(event) {
    return { type: eventContants.GET_EVENTEDIT_SUCCESS, event };
  }
  function failure(id, error) {
    return { type: eventContants.GET_EVENTEDIT_FAILURE, id, error };
  }
}

function createEvent(event) {
  return dispatch => {
    dispatch(request(event));

    eventService.createEvent(event).then(
      event => {
        dispatch(success(event));
        dispatch(alertActions.success('Event created!'));
        history.push('/');
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );

    // userService.tokenAuthentication().then(
    //     user => {
    //         const storedUser = JSON.parse(localStorage.getItem('user'));
    //         if (storedUser && user.id === storedUser.id) {

    //             eventService.createEvent(event)
    //             .then(
    //                 event => {
    //                     dispatch(success(event));
    //                     dispatch(alertActions.success('Event created!'));
    //                 },
    //                 error => {
    //                     dispatch(failure(error.toString()));
    //                     dispatch(alertActions.error(error.toString()));
    //                 }
    //             );

    //         } else {
    //             userService.logout();
    //             history.push('/');
    //         }
    //     },
    //     error => {
    //         dispatch(failure(error.toString()));
    //         dispatch(alertActions.error(error.toString()));
    //     }
    // );
  };

  function request(event) {
    return { type: eventContants.CREATE_EVENT_REQUEST, event };
  }
  function success(event) {
    return { type: eventContants.CREATE_EVENT_SUCCESS, event };
  }
  function failure(error) {
    return { type: eventContants.CREATE_EVENT_FAILURE, error };
  }
}

function updateEvent(event, id) {
  return dispatch => {
    dispatch(request(id));

    userService.tokenAuthentication().then(
      user => {
        const storedUser = JSON.parse(localStorage.getItem('user'));
        if (storedUser && user.id === storedUser.id) {
          eventService.updateEvent(event, id).then(
            event => {
              dispatch(success(event));
              dispatch(alertActions.success('Event updated!'));
            },
            error => {
              dispatch(failure(error.toString()));
              dispatch(alertActions.error(error.toString()));
            },
          );
        } else {
          userService.logout();
          history.push('/');
        }
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );
  };

  function request(id) {
    return { type: eventContants.UPDATE_EVENT_REQUEST, id };
  }
  function success(event) {
    return { type: eventContants.UPDATE_EVENT_SUCCESS, event };
  }
  function failure(error) {
    return { type: eventContants.UPDATE_EVENT_FAILURE, error };
  }
}

function deleteEvent(id) {
  return dispatch => {
    dispatch(request());

    eventService.deleteEvent(id).then(
      response => {
        dispatch(success(id));
        dispatch(alertActions.success('Event deleted!'));
        history.push('/');
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );

    // userService.tokenAuthentication().then(
    //     user => {
    //         const storedUser = JSON.parse(localStorage.getItem('user'));
    //         if (storedUser && user.id === storedUser.id) {
    //             eventService.deleteEvent(id)
    //             .then(
    //                 response => {
    //                     dispatch(success(id));
    //                     dispatch(alertActions.success('Event deleted!'));
    //                 },
    //                 error => {
    //                     dispatch(failure(error.toString()));
    //                     dispatch(alertActions.error(error.toString()));
    //                 }
    //             );
    //         } else {
    //             userService.logout();
    //             history.push('/');
    //         }
    //     },
    //     error => {
    //         dispatch(failure(error.toString()));
    //         dispatch(alertActions.error(error.toString()));
    //     }
    // );
  };

  function request() {
    return { type: eventContants.DELETE_EVENT_REQUEST };
  }
  function success(id) {
    return { type: eventContants.DELETE_EVENT_SUCCESS, id };
  }
  function failure(error) {
    return { type: eventContants.DELETE_EVENT_FAILURE, error };
  }
}
