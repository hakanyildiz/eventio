import { userConstants } from '../_constants';
import { alertActions } from '.';
import { history } from '../_helpers';
import { userService } from '../_services';

export const userActions = {
  login,
  logout,
  register,
};

function login(email, password) {
  return dispatch => {
    dispatch(request({ email }));

    userService.userAuthentication(email, password).then(
      user => {
        dispatch(success(user));
        history.push('/');
      },
      error => {
        // MOCK ERROR. When I was trying to authenticate with wrong username or password,
        // Server has returned only 400 Bad Request.

        let err = '';
        if (error.toString() === 'Bad Request') {
          err = 'AuthError';
        } else {
          err = error.toString();
        }

        dispatch(failure(err));
        dispatch(alertActions.error(err));
      },
    );
  };
  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    userService.register(user).then(
      user => {
        dispatch(success());
        history.push('/login');
        dispatch(alertActions.success('Registration succesful'));
      },
      error => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      },
    );
  };

  function request(user) {
    return { type: userConstants.REGISTER_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.REGISTER_SUCCESS, user };
  }

  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }
}
