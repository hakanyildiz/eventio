export function authHeader() {
  const token = localStorage.getItem('AccessToken');
  if (token) {
    return { Authorization: token };
  }
  return {};
}
/* 
    AccessToken Getter&Setter
*/
export function getAccessToken() {
  const token = localStorage.getItem('AccessToken');
  if (token) {
    return token;
  }
  return null;
}

export function setAccessToken(token) {
  if (token) {
    localStorage.setItem('AccessToken', token);
  }
}

/* 
    RefreshToken Getter&Setter
*/
export function getRefreshToken() {
  const token = localStorage.getItem('RefreshToken');
  if (token) {
    return token;
  }
  return null;
}

export function setRefreshToken(token) {
  if (token) {
    localStorage.setItem('RefreshToken', token);
  }
}
