import { alertConstants } from '../_constants';

export function alert(state = {}, action) {
  switch (action.type) {
    case alertConstants.SUCCESS:
      return {
        type: 'success',
        message: action.message,
        id: JSON.stringify(new Date()),
      };
    case alertConstants.ERROR:
      return {
        type: 'error',
        message: action.message,
        id: JSON.stringify(new Date()),
      };
    case alertConstants.CLEAR:
      return {};
    default:
      return state;
  }
}
