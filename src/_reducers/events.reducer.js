import { eventContants } from '../_constants';

export function events(
  state = { items: [], loading: false, itemView: null, itemEdit: null },
  action,
) {
  switch (action.type) {
    case eventContants.GET_ALLEVENTS_REQUEST:
      return {
        loading: true,
      };
    case eventContants.GET_ALLEVENTS_SUCCESS:
      return {
        ...state,
        items: action.events,
        loading: false,
      };
    case eventContants.GET_ALLEVENTS_FAILURE:
      return {
        error: action.error,
      };
    case eventContants.GET_EVENTVIEW_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case eventContants.GET_EVENTVIEW_SUCCESS:
      return {
        ...state,
        itemView: action.event,
        loading: false,
      };
    case eventContants.GET_EVENTVIEW_FAILURE:
      return {
        error: action.error,
      };
    case eventContants.GET_EVENTEDIT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case eventContants.GET_EVENTEDIT_SUCCESS:
      return {
        ...state,
        itemEdit: action.event,
        loading: false,
      };
    case eventContants.GET_EVENTEDIT_FAILURE:
      return {
        error: action.error,
      };
    case eventContants.CREATE_EVENT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case eventContants.CREATE_EVENT_SUCCESS:
      if (state.items.length === 0) {
        return {
          ...state,
          loading: false,
        };
      }
      return {
        ...state,
        items: [...state.items, action.event],
        loading: false,
      };
    case eventContants.CREATE_EVENT_FAILURE:
      return {
        error: action.error,
      };
    case eventContants.UPDATE_EVENT_REQUEST:
      return {
        ...state,
        loading: true,
        id: action.id,
      };
    case eventContants.UPDATE_EVENT_SUCCESS:
      return {
        ...state,
        items: [
          ...state.items.filter(event => event.id !== action.event.id),
          Object.assign({}, action.event),
        ],
        loading: false,
      };
    case eventContants.UPDATE_EVENT_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    case eventContants.DELETE_EVENT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case eventContants.DELETE_EVENT_SUCCESS:
      if (state.items.length === 0) {
        return {
          ...state,
          loading: false,
          itemView: null,
          itemEdit: null,
        };
      }
      return {
        ...state,
        items: [...state.items.filter(event => event.id !== action.id)],
        itemView: null,
        itemEdit: null,
        loading: false,
      };
    case eventContants.DELETE_EVENT_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    case eventContants.ATTEND_EVENT_SUCCESS:
      if (state.items.length === 0) {
        return {
          ...state,
          itemView: action.event,
        };
      }
      return {
        ...state,
        itemView: action.event,
        items: [
          ...state.items.filter(event => event.id !== action.event.id),
          Object.assign({}, action.event),
        ],
      };
    case eventContants.UNATTEND_EVENT_SUCCESS:
      if (state.items.length === 0) {
        return {
          ...state,
          itemView: action.event,
        };
      }
      return {
        ...state,
        itemView: action.event,
        items: [
          ...state.items.filter(event => event.id !== action.event.id),
          Object.assign({}, action.event),
        ],
      };
    case eventContants.EVENTS_RESET:
      return {};
    default:
      return state;
  }
}
