import { combineReducers } from 'redux';

import { authentication } from './authenticaton.reducer';
import { registration } from './registration.reducer';
import { alert } from './alert.reducer';
import { events } from './events.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  alert,
  events,
});

export default rootReducer;
