import { authHeader, config } from '../_helpers';
import { userService }  from './user.service';
function getAllEvents() {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
  };

  return fetch(`${config.apiUrl}/events`, requestOptions).then(handleResponse);
}

function getEventById(id) {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
  };

  return fetch(`${config.apiUrl}/events/${id}`, requestOptions).then(
    handleResponse,
  );
}

function createEvent(event) {
  const requestOptions = {
    method: 'POST',
    headers: {
      ...authHeader(),
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
    body: JSON.stringify(event),
  };

  return fetch(`${config.apiUrl}/events`, requestOptions).then(handleResponse);
}

function updateEvent(event, id) {
  const requestOptions = {
    method: 'PATCH',
    headers: {
      ...authHeader(),
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
    body: JSON.stringify(event),
  };

  return fetch(`${config.apiUrl}/events/${id}`, requestOptions).then(
    handleResponse,
  );
}

function deleteEvent(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: {
      ...authHeader(),
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
  };

  return fetch(`${config.apiUrl}/events/${id}`, requestOptions).then(
    handleDeleteResponse,
  );
}

function attendEvent(id) {
  const requestOptions = {
    method: 'POST',
    headers: {
      ...authHeader(),
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
  };
  return fetch(
    `${config.apiUrl}/events/${id}/attendees/me`,
    requestOptions,
  ).then(handleResponse);
}

function unattendEvent(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: {
      ...authHeader(),
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
  };

  return fetch(
    `${config.apiUrl}/events/${id}/attendees/me`,
    requestOptions,
  ).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);

    if (!response.ok) {
      let error = (data && data.message) || response.statusText;

      if (response.status === 400) {
        userService.tokenAuthentication();
        error = 'Token Error. Please try your action again!';
      }


      return Promise.reject(error);
    }
    return data;
  });
}

function handleDeleteResponse(response) {
  if (response.status === 204) {
    return { message: 'success' };
  }
  let error = response.statusText;
  if (response.status === 400) {
    userService.tokenAuthentication();
    error = 'Token Error. Please try your action again!';
  }
  
  return Promise.reject(error);
}

export const eventService = {
  getAllEvents,
  getEventById,
  createEvent,
  updateEvent,
  deleteEvent,
  attendEvent,
  unattendEvent,
};
