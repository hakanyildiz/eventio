import { config } from '../_helpers';
import { getRefreshToken, setAccessToken, setRefreshToken } from '../_helpers';

export const userService = {
  userAuthentication,
  tokenAuthentication,
  logout,
  register,
};

function tokenAuthentication() {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
    body: JSON.stringify({ refreshToken: getRefreshToken() }),
  };

  return fetch(`${config.apiUrl}/auth/native`, requestOptions).then(
    handleResponse,
  );
}

function userAuthentication(email, password) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
    body: JSON.stringify({ email, password }),
  };

  return fetch(`${config.apiUrl}/auth/native`, requestOptions)
    .then(handleResponse)
    .then(user => {
      if (user.id) {
        localStorage.setItem('user', JSON.stringify(user));
      }
      return user;
    });
}

function logout() {
  localStorage.removeItem('user');
  localStorage.removeItem('AccessToken');
  localStorage.removeItem('RefreshToken');
}

function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      APIKey: config.apiKey,
    },
    body: JSON.stringify(user),
  };

  return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then(text => {
    response.headers.forEach((value, name) => {
      if (name.toLowerCase() === 'authorization') {
        setAccessToken(value);
      } else if (name.toLowerCase() === 'refresh-token') {
        setRefreshToken(value);
      }
    });
    console.log('aga bee döndü', response);
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        logout();
        window.location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
