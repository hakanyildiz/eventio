import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { eventActions } from '../_actions';
import ListEvent from './ListEvent';
import EventActionButton from './EventActionButton';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    tabsIndicator: {
        backgroundColor: '#f9f9fb',
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: 30,
        color: '#a9aeb4',
        fontWeight: '600',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '1px',
        lineHeight: '2',
        '&:hover': {
          color: '#323c46',
          opacity: 1,
        },
        '&$tabSelected': {
          color: '#323c46',
        },
        '&:focus': {
          color: '#323c46',
        },
    },
    dropdownIcon: {
        marginButtom: '5px',
    },
    dropdownButton: {
        fontSize: '12px',
        fontWeight: '600',
        lineHeight: '2',
        letterSpacing: '1px',
        color: '#323c46',
        paddingLeft: '6px'
    },
    tabSelected: {}
});

class AllEvents extends Component {
    state = {
        viewId: 0,
        tabSelectedIndex: 0,
        selectedTab: 'ALL EVENTS',
        anchorEl: null
    }

    handleMobileDropdown = event => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleCloseMobileDropdown = () => {
        this.setState({ anchorEl: null});
    }

    componentDidMount() {
        const { dispatch,events } = this.props;

        if(events && events.items && events.items.length > 0) {
            
        } else {
            dispatch(eventActions.getAllEvents());
        }
        
    }

    previewItem (id) {
        this.setState({redirect: true, viewId: id});
    }
     
    handleTabChange = (event, value) => {
        
        let selectedTab = 'ALL EVENTS';
        if (value === 1) selectedTab = 'FUTURE EVENTS';
        else if(value === 2) selectedTab = 'PAST EVENTS'; 

        this.setState({
            tabSelectedIndex: value,
            selectedTab
        });

        this.handleCloseMobileDropdown();
    }


    renderTabs(){
        const { classes } = this.props;
        const { tabSelectedIndex, selectedTab, anchorEl } = this.state;

        return (
            <div className="tab-container layout horizontal">
                    <div className="events-tabs">
                        <Tabs
                            value={tabSelectedIndex}
                            onChange={this.handleTabChange}
                            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                            >
                            <Tab 
                                label="ALL EVENTS"
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                            />
                            <Tab 
                                label="FUTURE EVENTS"
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                            />
                            <Tab 
                                label="PAST EVENTS"
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                            />
                        </Tabs>
                    </div>
                    <div className="mobile-events-tabs layout horizontal center">
                        <span className="show">SHOW: </span>
                        <Button
                            aria-owns={anchorEl ? 'simple-menu' : null}
                            aria-haspopup="true"
                            onClick={this.handleMobileDropdown}
                            className={classes.dropdownButton}
                            >
                            {selectedTab}
                            <ArrowDropDownIcon className={classes.dropdownIcon} />
                        </Button>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            open={Boolean(anchorEl)}
                            onClose={this.handleCloseMobileDropdown}
                            >
                                <MenuItem onClick={() => this.handleTabChange(this, 0)}>All Events</MenuItem>
                                <MenuItem onClick={() => this.handleTabChange(this, 1)}>Future Events</MenuItem>
                                <MenuItem onClick={() => this.handleTabChange(this, 2)}>Past Events</MenuItem>
                        </Menu>
                    </div>
                </div>
        );
    }
   
    render() { 
        const { selectedTab} = this.state;
        return (
            <div className="events-subpage">
                { this.renderTabs() }
                
                <ListEvent filter={ selectedTab.split(' ')[0].toLowerCase() }/>
                <EventActionButton />
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { events } = state;
    return {
        events
    }
}

AllEvents.propTypes = {
    classes: PropTypes.object.isRequired,
    events: PropTypes.object.isRequired
}

const connectedAllEvents = connect(mapStateToProps)(AllEvents);

export default withStyles(styles)(connectedAllEvents);