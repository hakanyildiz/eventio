import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const AttendeeCard = props => {
  const { item, userId } = props;
  const renderAttendes = () => {
    if (
      item.attendees.filter(attendee => attendee.id !== userId).length === 0
    ) {
      return <span>No one have attended yet!</span>;
    }
    return item.attendees.map((attendee, index) => {
      if (attendee.id === userId) {
        return (
          <li key={index} className="you">
            You
          </li>
        );
      }
      return (
        <li key={index}>
          {attendee.firstName} {attendee.lastName}
        </li>
      );
    });
  };
  return (
    <div className="event-card attendees-card">
      <h3 className="title">Attendees</h3>
      <div className="list">{renderAttendes()}</div>
    </div>
  );
};

const mapStateToProps = state => {
  const { authentication } = state;
  const { user } = authentication;
  return {
    userId: user.id,
  };
};

AttendeeCard.propTypes = {
  item: PropTypes.object.isRequired,
  userId: PropTypes.string.isRequired,
};

AttendeeCard.defaultProps = {
  item: null,
  userID: '0',
};

export default connect(mapStateToProps)(AttendeeCard);
