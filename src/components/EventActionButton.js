import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Done';
import { history } from '../_helpers';

const styles = theme => ({
  addbutton: {
    margin: theme.spacing.unit,
    color: '#ffffff',
    backgroundColor: '#323c46',
    '&:hover': {
      backgroundColor: '#555d5a',
    },
  },
  savebutton: {
    margin: theme.spacing.unit,
    color: '#ffffff',
    backgroundColor: '#22d486',
    '&:hover': {
      backgroundColor: '#20be78',
    },
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

const EventActionButton = props => {
  const { classes, action } = props;

  if (action === 'save') {
    return (
      <div className="action-button">
        <Button
          variant="fab"
          color="primary"
          aria-label="Save Event"
          className={classes.savebutton}
          onClick={props.handleSave}
        >
          <SaveIcon />
        </Button>
      </div>
    );
  }

  return (
    <div className="action-button">
      <Button
        variant="fab"
        color="primary"
        aria-label="Add Event"
        className={classes.addbutton}
        // onClick={() => handleClick()}>
        onClick={() =>
          history.push('/events/create', { from: history.location.pathname })
        }
      >
        <AddIcon />
      </Button>
    </div>
  );
};

EventActionButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EventActionButton);
