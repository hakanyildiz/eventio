import React from 'react';
import { connect } from 'react-redux';
import UserIcon from '@material-ui/icons/PermIdentitySharp';
import { Link } from 'react-router-dom';
import { eventActions } from '../_actions';
import Moment from 'react-moment';
import momentJs from 'moment';


const EventCard = props => {
    const { index, event, user, extended } = props;
    const now = momentJs().format("YYYY-MM-DD HH:mm:ss"); 
    const handleUserAction = (action) => {
        const res = window.confirm(`Are you sure to ${action} the event?`)
        if(res) {
            if ( action === 'join') {
                props.dispatch(eventActions.attendEvent(event.id));
            } else if ( action === 'leave') {
                props.dispatch(eventActions.unattendEvent(event.id));
            }
        }
    };

    const renderButton = () => {
        if (now > momentJs(event.startsAt).format("YYYY-MM-DD HH:mm:ss")) {
            return (
                <button className="btn-event" name="full" disabled>Completed</button> 
            );
        } else {
            if (event.capacity === event.attendees.length) {
                return(
                    <button className="btn-event" name="full" disabled>Full</button> 
                );
            }

            if (user.id === event.owner.id) {
                if (!extended) {
                    return (
                        <button className="btn-event btn-edit" name="edit">Edit</button> 
                    );
                } 
                if (extended) {
                    return (
                        <Link to={`/events/${event.id}/edit`}>
                            <button className="btn-event btn-edit" name="edit">Edit</button> 
                        </Link>
                    );
                }
            } 
            else {
                if (event.attendees.filter(attendee => attendee.id === user.id).length === 0) {
                    return (
                        <button className="btn-event btn-join" name="join" onClick={(e) => handleUserAction('join',e)}>Join</button> 
                    );
                }
                else if(event.attendees.filter(attendee => attendee.id === user.id).length > 0) {
                    return (
                        <button className="btn-event btn-leave" name="leave" onClick={(e) => handleUserAction('leave',e)}>Leave</button> 
                    );
                }
            }

        }
    };
   
  return (
    <div
      className={'event-card layout vertical' + (extended ? ' extended' : '')}
      id={`event-${index}`}
    >
      <div className="date">
                <Moment format="MMMM D, YYYY - h:mm a">{event.startsAt}</Moment>
      </div>
            <div className="title">{event.title}</div>
      <div className="owner">
        {event.owner.firstName} {event.owner.lastName}
      </div>
            <div className="description">{event.description}</div>
            <div className="buttons layout horizontal">
        <div
          className="flex layout horizontal center"
          style={{ color: '#949ea8' }}
        >
          <UserIcon color="inherit" />
                    <span className="attendees">{`${event.attendees.length} of ${event.capacity}`}</span>
                </div>
                <div>
                    { renderButton() }
                </div>
                
            </div>
        </div>
    )
};

const mapStateToProps = state => {
  const { authentication } = state;
    const { user } = authentication;
  return {
        user
    }
}


export default connect(mapStateToProps)(EventCard);


