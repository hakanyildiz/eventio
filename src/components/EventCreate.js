import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { eventActions } from '../_actions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import TextField from '@material-ui/core/TextField';
import Moment from 'moment';
import { ButtonLoadingDataImage } from '../_helpers';

class EventCreate extends React.Component {

    constructor(props){
        super(props);
        
        this.state = {
            event: {
                title: '',
                description: '',
                date: Moment().format('YYYY-MM-DD'),
                time: '07:30',
                capacity: '0'
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        ValidatorForm.addValidationRule('isCapacityValid', (value) => {
            if (value <= 0) {
                return false;
            }
            return true;
        });
    }

    handleChange = name => e => {
        const { event } = this.state;

        this.setState({
            event: {
                ...event,
                [name]: e.target.value
            }
        });
        
    };

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });

        const { event } = this.state;
        const { dispatch } = this.props;
        

        if(event.title && event.description && event.date && event.time && event.capacity
        ) {
            const startsAt = `${event.date}T${event.time}:05.325Z`;
            const now = Moment().format();
            if(now > startsAt) {
                alert('You can NOT create an old-dated event!');
                return;
            }

            let creatingEvent = {
                title: event.title,
                description: event.description,
                capacity: event.capacity,
                startsAt: startsAt
            };
            dispatch(eventActions.createEvent(creatingEvent));
        }
    }

    render() {
        const { loading, location } = this.props;
        const { event } = this.state;
        const error = '';
        const redirectPath = location.state.from;
        return (
            <div className="events-subpage layout vertical center-center">
                <Link to={redirectPath} className="close-create-event">
                    <CloseIcon />
                    <span>Close</span>
                </Link>
                
                <div className="event-card create-card">
                        <h1 className="title">Create new Event</h1>
                        <span className={'subtitle' + (error ? ' alert' : '')}>
                            { !error ? 'Enter details below.': error }
                        </span>

                        <ValidatorForm
                            onSubmit={this.handleSubmit}
                            className="layout vertical"
                            >
                            <div className="form-group">     
                                <TextValidator
                                    fullWidth   
                                    label="Title"
                                    onChange={this.handleChange('title')}
                                    name="title"
                                    value={event.title}
                                    validators={['required']}
                                    errorMessages={['Title has to be filled up']}
                                    aria-describedby="title" 
                                />
                            </div>
                            <div className="form-group">     
                                <TextValidator
                                    label="Description"
                                    onChange={this.handleChange('description')}
                                    name="description"
                                    value={event.description}
                                    validators={['required']}
                                    errorMessages={['Description has to be filled up']}
                                    aria-describedby="description" 
                                    fullWidth
                                />
                            </div>
                            <div className="form-group">     
                                <TextField
                                    id="date"
                                    label="Date"
                                    type="date"
                                    value={event.date}
                                    onChange={this.handleChange('date')}
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    fullWidth
                                />
                            </div>
                            <div className="form-group">     
                                <TextField
                                    id="time"
                                    label="Time"
                                    type="time"
                                    value={event.time}
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    inputProps={{
                                    step: 300, // 5 min
                                    }}
                                    fullWidth
                                    onChange={this.handleChange('time')}
                                    />

                            </div>
                            <div className="form-group">    
                                <TextValidator
                                    label="Capacity"
                                    onChange={this.handleChange('capacity')}
                                    name="capacity"
                                    type="number"
                                    validators={['required', 'isCapacityValid']}
                                    errorMessages={['Capacity has to be filled up', 'You should add a valid capacity']}
                                    value={event.capacity}
                                    aria-describedby="capacity" 
                                    fullWidth
                                    InputLabelProps={{
                                        shrink: true,
                                      }}
                                />
                            </div>
                            <div className="form-group" style={{ marginTop: '50px' }}>
                                <button className="btn-submit layout center-center">
                                    {!loading && <span>CREATE NEW EVENT</span> }
                                    {loading &&
                                        <img alt="loading gif" src={ButtonLoadingDataImage} />
                                    }
                                </button>
                            </div>
                        </ValidatorForm>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
 
    const { events } = state;
    return {
        loading: events.loading
    };
}

EventCreate.propTypes = {
    loading: PropTypes.bool
}

EventCreate.defaultProps = {
    loading: false
}


export default connect(mapStateToProps)(EventCreate);


