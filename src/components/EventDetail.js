import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ErrorIcon from '@material-ui/icons/Error';
import ArrowLeft from '@material-ui/icons/ArrowBackTwoTone';
import { eventActions } from '../_actions';
import LoadingGif from '../assets/images/loading.gif';

import EventCard from './EventCard';
import AttendeeCard from './AttendeeCard';
import EventActionButton from './EventActionButton';

class EventDetail extends Component {
  componentDidMount() {
    const { match, item } = this.props;

    if (!item || (item && item.id && item.id !== match.params.id)) {
      this.props.dispatch(eventActions.getEventView(match.params.id));
    }
  }

  renderAttendes() {
    const { item, user } = this.props;
    if (
      item.attendees.filter(attendee => attendee.id !== user.id).length === 0
    ) {
      return <span>No one has attended yet!</span>;
    }
    return item.attendees.map((attendee, index) => (
      <li key={index}>
        {attendee.firstName} {attendee.lastName}
      </li>
    ));
  }

  render() {
    const { item, loading, match } = this.props;

    if (!item && loading) {
      return (
        <div className="events-subpage">
          <div className="events-subpage-loading">
            <img src={LoadingGif} width="300" height="300" alt="Loading Gif" />
          </div>
        </div>
      );
    }

    if (!item) {
      return (
        <div className="events-subpage">
          <Link to="/events" className="back-events-link">
            <ArrowLeft />
            <span>Back to Events</span>
          </Link>
          <div className="event-card">
            <div className="title">
              <ErrorIcon />
              <span>
                No event found for <b>{match.params.id}</b>
              </span>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="events-subpage">
        <Link to="/events" className="back-events-link">
          <ArrowLeft />
          <span>Back to Events</span>
        </Link>
        <div className="header">
          <h2 className="event-id">DETAIL EVENT: #{item.id}</h2>
        </div>
        <div className="content layout horizontal">
          <div className="flex-8">
            <EventCard index="0" event={item} extended="true" />
          </div>
          <div className="flex-4">
            <AttendeeCard item={item} />
          </div>
        </div>
        <EventActionButton />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { authentication, events } = state;
  const { user } = authentication;
  return {
    user,
    item: events.itemView,
    loading: events.loading,
  };
};

EventDetail.propTypes = {
  item: PropTypes.object,
  match: PropTypes.object.isRequired,
  loading: PropTypes.bool,
};

EventDetail.defaultProps = {
  item: null,
  loading: false,
};

const connectedEventDetail = connect(mapStateToProps)(EventDetail);

export default connectedEventDetail;
