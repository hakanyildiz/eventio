import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import LoadingGif from '../assets/images/loading.gif';

import { eventActions } from '../_actions';

import AttendeeCard from './AttendeeCard';
import EventActionButton from './EventActionButton';

import ArrowLeftIcon from '@material-ui/icons/ArrowBackTwoTone';
import ErrorIcon from '@material-ui/icons/Error';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Moment from 'moment';

import { ValidatorForm } from 'react-material-ui-form-validator';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

class EventEdit extends Component {

    constructor(props) {
        super(props);

        this.state = {
            event: {
                title: props.item && props.item.title,
                description: props.item && props.item.description,
                capacity: props.item && props.item.capacity,
                date: props.item && Moment(props.item.startsAt).format('YYYY-MM-DD'),
                time: props.item && Moment(props.item.startsAt).format('HH:mm'),
                submitted: false
            }
        };

        //Using ref to trigger button click
        this.btnSubmit = React.createRef();

        this.handleChange = this.handleChange.bind(this);
        this.handleDeleteEvent = this.handleDeleteEvent.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = name => e => {
        const { event } = this.state;

        this.setState({
            event: {
                ...event,
                [name]: e.target.value
            }
        });
        
    };

    componentDidMount() {
        const { match, dispatch } = this.props;
        dispatch(eventActions.getEventEdit(match.params.id));
    }

    componentWillReceiveProps(nextProps) {  

        if (this.props.item !== nextProps.item && nextProps.item) {
            const { title, description, capacity, startsAt } = nextProps.item;
            this.setState({
                event: {
                    title,
                    description,
                    capacity,
                    date: Moment(startsAt).format('YYYY-MM-DD'),
                    time: Moment(startsAt).format('HH:mm'),
                }
            });
        }   
    }

    handleDeleteEvent(e) {
        const { match, dispatch } = this.props;
        
        const id = match.params.id;
 
        if (id) {
            const res = window.confirm(`Are you sure to delete ${id} event?`);
            if(res === true) {
                dispatch(eventActions.deleteEvent(id));
            }
        }

    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });

        const { event } = this.state;
        const { dispatch, item } = this.props;

        if(event.title && event.description && event.date && event.time && event.capacity) {
            
            let updatingEvent = {
                title: event.title,
                description: event.description,
                capacity: event.capacity,
                startsAt: `${event.date}T${event.time}:05.325Z`
            };
            dispatch(eventActions.updateEvent(updatingEvent, item.id));
        }
    }

    render() {
        const { item, loading, match, user} = this.props;
        const { event, submitted } = this.state;

        if (!item) {

            if (loading) {
                return (
                    <div className="events-subpage-loading">
                        <img src={LoadingGif} width="300" height="300" alt="Loading Gif" />
                    </div>
            )}

            return (
                <div className="events-subpage">
                    <Link to="/events" className="back-events-link">
                        <ArrowLeftIcon />
                        <span>Back to Events</span>
                    </Link>
                    <div className="event-card">
                        <div className="title">
                            <ErrorIcon />
                            <span>
                                No event found for <b>{match.params.id}</b> 
                            </span>
                        </div>
                    </div>
                </div>
            );
        } else {
            if (item && (user.id !== item.owner.id)) {
                return (
                    <div className="events-subpage">
                        <Link to="/events" className="back-events-link">
                            <ArrowLeftIcon />
                            <span>Back to Events</span>
                        </Link>
                        <div className="event-card">
                            <div className="title">
                                <ErrorIcon />
                                <span>
                                    You busted! The event does not belongs to you ;)
                                </span>
                            </div>
                        </div>
                    </div>
                );
            }
            return (
                <div className="events-subpage">
                    <Link to="/events" className="back-events-link">
                        <ArrowLeftIcon />
                        <span>Back to Events</span>
                    </Link>
                    <div className="header layout horizontal">
                        <h2 className="event-id">DETAIL EVENT: #{ item.id }</h2>
                        <div className="flex"></div>
    
                        <Button onClick={this.handleDeleteEvent} className="btn-delete-event">
                            <DeleteIcon />
                            <span className="title">DELETE EVENT</span>
                        </Button>
                    </div>
                    <div className="content layout horizontal">
                        <div className="flex-8">
                            <div className="event-card edit-card">
                            <ValidatorForm
                                onSubmit={this.handleSubmit}
                                className="layout vertical"
                                >
                                <div className="form-group">
                                    
                                    <FormControl fullWidth error={ submitted && !event.date }>
                                        <InputLabel htmlFor="date">Date</InputLabel>
                                        <Input
                                            id="date"
                                            label="Date"
                                            type="date"
                                            value={event.date}
                                            onChange={this.handleChange('date')}
                                        />
                                        {   
                                            submitted && !event.date && (
                                                <FormHelperText id="date">Date has to be filled up</FormHelperText>
                                            )
                                        }  
                                    </FormControl>
                                </div>
                                <div className="form-group">
                                    <FormControl fullWidth error={ submitted && !event.time }>
                                        <InputLabel htmlFor="date">Time</InputLabel>
                                        <Input
                                            id="time"
                                            label="Time"
                                            type="time"
                                            value={event.time}
                                            onChange={this.handleChange('time')}
                                        />
                                        {   
                                            submitted && !event.time && (
                                                <FormHelperText id="time">Time has to be filled up</FormHelperText>
                                            )
                                        }  
                                    </FormControl>
                                </div>
                                <div className="form-group">     
                                    <FormControl fullWidth error={ submitted && !event.title }>
                                        <InputLabel htmlFor="title">Title</InputLabel>
                                        <Input
                                            id="title"
                                            label="Title"
                                            type="title"
                                            value={event.title}
                                            onChange={this.handleChange('title')}
                                        />
                                        {   
                                            submitted && !event.title && (
                                                <FormHelperText id="title">Title has to be filled up</FormHelperText>
                                            )
                                        }  
                                    </FormControl>
                                </div>
                                <div className="form-group">
                                    <FormControl fullWidth error={ submitted && !event.description }>
                                        <InputLabel htmlFor="description">Description</InputLabel>
                                        <Input
                                            id="description"
                                            label="Description"
                                            type="description"
                                            value={event.description}
                                            onChange={this.handleChange('description')}
                                        />
                                        {   
                                            submitted && !event.description && (
                                                <FormHelperText id="description">Description has to be filled up</FormHelperText>
                                            )
                                        }  
                                    </FormControl>     
                                </div>
                                <div className="form-group">
                                    <FormControl fullWidth error={ submitted && !event.capacity }>
                                        <InputLabel htmlFor="capacity">Capacity</InputLabel>
                                        <Input
                                            id="capacity"
                                            label="Capacity"
                                            type="capacity"
                                            value={event.capacity}
                                            onChange={this.handleChange('capacity')}
                                        />
                                        {   
                                            submitted && !event.capacity && (
                                                <FormHelperText id="capacity">Capacity has to be filled up</FormHelperText>
                                            )
                                        }  
                                    </FormControl>    
                                   
                                </div>
                                <button onClick={this.handleSubmit} ref={this.btnSubmit} hidden></button>
                            </ValidatorForm>
                            </div>
                        </div>
                        <div className="flex-4">
                            <AttendeeCard item={item} />
                        </div>
                    </div>
                    <EventActionButton action="save" handleSave={ () => this.btnSubmit.current.click() }/>
                </div>
            );


        } //else

    }

}

const mapStateToProps = state => {
 
    const { authentication, events } = state;
    const { user } = authentication;
    return {
        user,
        item: events.itemEdit,
        loading: events.loading
    };
}

EventEdit.propTypes = {
    item: PropTypes.object,
    match: PropTypes.object.isRequired,
    loading: PropTypes.bool
}

EventEdit.defaultProps = {
    item: null,
    loading: false
}

const connectedEventEdit = connect(mapStateToProps)(EventEdit);

export default connectedEventEdit;
