import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../assets/images/logo-black.svg';
import UserButton from './UserButton';

const Header = ({ match }) => (
  <div className="header-container layout horizontal center">
    <Link to="/" className="logo">
      <img width="30" height="30" src={Logo} alt="Eventio Logo" />
    </Link>
    <div className="flex" />
    <UserButton />
  </div>
);

export default Header;
