import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import ViewStreamIcon from '@material-ui/icons/ViewStream';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import LoadingGif from '../assets/images/loading.gif';
import moment from 'moment';
import EventCard from './EventCard';
import { Link } from 'react-router-dom';
import { eventActions } from '../_actions';

//////// Filtering Events
//<ListEvent filter="all" />
//<ListEvent filter="past" />
//<ListEvent filter="future" />
//////// Getting Events of Spesific User 
//<ListEvent userId="######id"

class ListEvent extends Component {
    
    state = {
        selectedView: 'grid-view',
        redirect: false,
    }

    componentDidMount() {
        const { events, dispatch } = this.props;

        if ( events && events.items && !events.error && events.items.length === 0)
        {
            dispatch(eventActions.getAllEvents());
        }
    }
    
    handleSelectedView(view) {
        const { selectedView } = this.state;

        if ( selectedView !== view ) {
            const nextView = (selectedView === 'flex-view') ? 'grid-view' : 'flex-view';
        
            this.setState({
                selectedView: nextView
            });
        }

    }

    renderList(tabSelectedIndex) {
        const { events, userId, filter } = this.props;
        let { items } = events;
        const now = moment().format("YYYY-MM-DD HH:mm:ss");

        if ( userId ) {
            items = items.filter(item => item.owner.id === userId);

            if (items.length === 0) {
                return (
                    <h2 className="message">
                        You haven't created any event yet!
                    </h2>
                )
            }
        }
        //compare now and start date
        if (filter === 'future') { 
            items = items.filter(item => moment(item.startsAt).format("YYYY-MM-DD HH:mm:ss") > now)
        }
        else if (filter === 'past') { 
            items = items.filter(item => now > moment(item.startsAt).format("YYYY-MM-DD HH:mm:ss"));
        }

        if (items.length === 0) {
            let message = `No ${filter === 'all' ? '' : filter } events are found!`;
            return (
                <h2 className="message">
                    {message}
                </h2>
            );
        }

        return ( items.map((event, index) => (
            <Link key={index}  to={`events/${event.id}/detail`}>
                <EventCard index={index} event={event} />
            </Link>
            )
        ));
    }

    render() {
        const { selectedView } = this.state;
        const { tabSelectedIndex, events } = this.props;
        return (
            <div className="list-event-container">
                <div className="layout-buttons">
                    <IconButton
                        onClick={() => this.handleSelectedView('grid-view')}
                        className={'list-grid-view' + (selectedView === 'grid-view' ? ' selected': '' ) }
                    >
                        <ViewModuleIcon />
                    </IconButton>
                    <IconButton 
                        onClick={() => this.handleSelectedView('flex-view')}
                        className={'list-grid-view' + (selectedView === 'flex-view' ? ' selected': '' ) }
                    >
                        <ViewStreamIcon />
                    </IconButton>
                </div>
                <div className={'all-events ' + selectedView}>
                        {!events.items && events.loading && 
                            <div className="loading-container">
                                <img src={LoadingGif} width="300" height="300" alt="Loading Gif"/>
                            </div>
                        }
                        {!events.items && events.error && 
                            <span className="text-danger">ERROR: {events.error}</span>
                        }
                        {events.items &&
                           ( this.renderList(tabSelectedIndex) )
                        }   
                    </div>
    
            </div>
        );
    }
}


function mapStateToProps(state) {
    const { events } = state;
    return {
        events,
    }
}

ListEvent.propTypes = {
    events: PropTypes.object.isRequired,
    userId: PropTypes.string,
    filter: PropTypes.string,
}

ListEvent.defaultProps = {
    events: null,
    filter: 'all'
}

export default connect(mapStateToProps)(ListEvent);