import React from 'react';
import Logo from '../assets/images/logo.svg';

const SignBanner = () => (
  <div className="banner flex-4 layout vertical">
    <div className="flex">
      <img
        className="logo noselect"
        height="30"
        width="30"
        src={Logo}
        alt="Eventio Logo"
      />
    </div>
    <div className="qoutes layout vertical center noselect">
      <div className="title">“Great, kid. Don’t get cocky.”</div>
      <div className="line" />
      <div className="author">Han Solo</div>
    </div>
  </div>
);

export default SignBanner;
