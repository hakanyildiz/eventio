import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';

class UserButton extends Component {

    state = {
        anchorEl: null,
    }   

    handleClick = event => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: null});
    }

    render(){
        const { user } = this.props;
        const { anchorEl } = this.state;

        return (
            <div className="user-button-container layout horizontal center">
                <div className="circle">
                    {`${user.firstName.charAt(0)} ${user.lastName.charAt(0)}`}
               </div>
               <div className="username">
                   {user.firstName} {user.lastName}
               </div>
               <IconButton
                    aria-owns={anchorEl ? 'simple-menu' : null}
                    aria-haspopup="true"
                    onClick={this.handleClick}
               >
                    <ArrowDropDown />
               </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    <Link to="/profile">
                        <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                    </Link>
                    <Link to="/login">
                        <MenuItem onClick={this.handleClose}>Logout</MenuItem>
                    </Link>
                </Menu>
           </div>
        );
    }
}

const mapStateToProps = state => {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    }
}

UserButton.propTypes = {
    user: PropTypes.object.isRequired,
}

export default connect(mapStateToProps)(UserButton);