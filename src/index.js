import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './_helpers';
import App from './App';
import {
  createMuiTheme,
  MuiThemeProvider,
} from '../node_modules/@material-ui/core';

const THEME = createMuiTheme({
  typography: {
    fontFamily: '"Hind", "PlayfairDisplay", "Arial", sans-serif',
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    palette: {
      primary: '#ff7800',
    },
  },
});

const render = Component => {
  ReactDOM.render(
    <MuiThemeProvider theme={THEME}>
      <Provider store={store}>
        <Component />
      </Provider>
    </MuiThemeProvider>,
    document.getElementById('app'),
  );
};

render(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    const newApp = require('./App').default;
    render(newApp);
  });
}
