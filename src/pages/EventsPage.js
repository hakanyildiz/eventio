import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { PrivateRoute } from '../components/PrivateRoute';
import AllEvents from '../components/AllEvents';
import EventDetail from '../components/EventDetail';
import EventEdit from '../components/EventEdit';
import EventCreate from '../components/EventCreate';

import Header from '../components/Header';

const EventsPage = () => (
  <div id="events-page">
    <Header />
    <Switch>
      <PrivateRoute exact path="/" component={AllEvents} />
      <PrivateRoute exact path="/events" component={AllEvents} />
      <PrivateRoute exact path="/events/:id/detail" component={EventDetail} />
      <PrivateRoute exact path="/events/:id/edit" component={EventEdit} />
      <PrivateRoute exact path="/events/create" component={EventCreate} />
      <Redirect push to="/error" />
    </Switch>
  </div>
);

export default EventsPage;
