import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import SignBanner from '../components/SignBanner';
import { history } from '../_helpers';
import BackImage from '../assets/images/nomatchicon.png';

const NoMatchPage = ({ alert }) => (
  <div className="sign-container layout horizontal">
    <img src={BackImage} className="nomatch-backimage" alt="back logo" />
    <SignBanner />
    <div className="content flex-8 layout vertical center-center">
      <div className="signup-router">
        Don't have account?
        <Link to="/register" className="link">
          {' '}
          SIGN UP{' '}
        </Link>
      </div>
    </div>
    <div className="nomatch-card layout vertical">
      <h1 className="header">
        {alert && alert.type === 'alert-danger'
          ? 'Something went wrong.'
          : '404 Error - page not found'}
      </h1>
      <span className="notification">
        Seems like Darth Vader just hits our website and drops it down.
      </span>
      <span className="notification">
        Please press the refresh button and everything should be fine again.
      </span>
      <button
        className="layout center-center"
        onClick={() => {
          history.push('/');
        }}
      >
        }} ><span>REFRESH</span>
      </button>
    </div>
  </div>
);

const mapStateToProps = state => {
  const { alert } = state;
  return {
    alert,
  };
};
export default connect(mapStateToProps)(NoMatchPage);
