import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListEvent from '../components/ListEvent';
import Header from '../components/Header';

const ProfilePage = props => (
  <div id="profile-container">
    <Header />
    <section className="event-card user-card layout vertical center">
      <div className="logo layout vertical center-center">
        <span>
          {props.user.firstName.charAt(0)} {props.user.lastName.charAt(0)}
        </span>
      </div>
      <h2 className="name">
        {props.user.firstName} {props.user.lastName}
      </h2>
      <p className="email">{props.user.email}</p>
    </section>

    <section className="user-events">
      <h1>My Events</h1>
      <ListEvent userId={props.user.id} />
    </section>
  </div>
);

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user,
  };
}

ListEvent.propTypes = {
  user: PropTypes.object,
};

ListEvent.defaultProps = {
  user: null,
};

export default connect(mapStateToProps)(ProfilePage);
