import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import SignBanner from '../components/SignBanner';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import MobileLogo from '../assets/images/logo-black.svg';
import { history } from '../_helpers'

class ResetPasswordPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            submitted: false,
            loading: false,
            isSent: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
    };

    handleSubmit(e) {
        e.preventDefault();

        this.setState({
            loading: true
        });

        setTimeout(() => {
            this.setState({
                isSent: true,
                loading: false,
                email: ''
            })
        }, 2000);
    }

    render() {
        const { email, loading, isSent } = this.state;

        return (
            <div className="sign-container layout horizontal">
                <SignBanner />
                <div className="content flex-8 layout vertical center-center">
                    <Link to="/" className="mobile-logo">
                        <img src={MobileLogo} width="30" height="30" alt="mobile logo" />
                    </Link>
                    {isSent ? (
                        <div className="login-card layout vertical">
                        <h1 className="header">Great! <span> We sent your the reset link.</span></h1>
                        <span className="notification" style={{marginTop: '10px'}}>
                            Please check your email.
                        </span>
                        <div className="form-group" style={{marginTop: '30px'}}>
                            <button className="btn-submit layout center-center" onClick={() => history.push('/')}> 
                                <span>
                                    BACK TO HOMEPAGE 
                                </span>
                            </button>
                        </div>
                    </div>
                    ) : (
                        <div className="login-card layout vertical">
                        <h1 className="header">Forgot your password?</h1>
                        <span className="notification" style={{marginTop: '10px'}}>
                            Enter your email address below.
                        </span>
                        <ValidatorForm
                            onSubmit={this.handleSubmit}
                            className="layout vertical"
                        >
                            <div className="form-group">     
                                <TextValidator
                                        label="Email"
                                        onChange={this.handleChange('email')}
                                        name="email"
                                        value={email}
                                        validators={['required', 'isEmail']}
                                        errorMessages={['this field is required', 'Email is not valid']}
                                        aria-describedby="email" 
                                        fullWidth
                                    />
                            </div>
                            <div className="signup-router">
                                Don't have account? 
                                <Link to="/register" className="link">    SIGN UP </Link>
                            </div>
                            <div className="form-group" style={{marginTop: '30px'}}>
                                <button className="btn-submit layout center-center">
                                    {!loading && <span>REQUEST RESET LINK</span> }
                                    {loading &&
                                        <img alt="loading gif" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </button>
                            </div>
                        </ValidatorForm>
                    </div>
                    )}



                    
                </div>
            </div>
        );
    }
}

export default ResetPasswordPage;