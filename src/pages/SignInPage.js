import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions, eventActions } from '../_actions';
import { ButtonLoadingDataImage } from '../_helpers';
import SignBanner from '../components/SignBanner';
import MobileLogo from '../assets/images/logo-black.svg';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import FormHelperText from '@material-ui/core/FormHelperText';

class SignInPage extends Component {
    constructor(props) {
        super(props);

        //reset login status 
        this.props.dispatch(userActions.logout());

        //reset event details
        this.props.dispatch(eventActions.resetEvents());

        this.state = {
            email: '',
            password: '',
            submitted: false,
            showPassword: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        ValidatorForm.addValidationRule('isEmptyEmail', (value) => {
            if (!value) {
                return false;
            }
            return true;
        });
    }

    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });
    };

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password }= this.state;
        const { dispatch } = this.props;
        
        if(email && password) {
            dispatch(userActions.login(email.toLowerCase(), password));
        }
    }

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    }

    render() { 
        const { loggingIn, error } = this.props;
        const { email, password, submitted } = this.state;

        return (
            <div className="sign-container layout horizontal">
                <SignBanner />
                <div className="content flex-8 layout vertical center-center">
                    <Link to="/" className="mobile-logo">
                        <img src={MobileLogo} width="30" height="30" alt="mobile logo" />
                    </Link>
                    <div className="login-card layout vertical">
                        <h1 className="header">Sign in to Eventio.</h1>
                        <span className={'notification' + (error ? ' alert' : '')}>
                            { !error ? 'Enter your details below.': 'Oops! That email and pasword combination is not valid.' }
                        </span>
                        <ValidatorForm
                            ref="form"
                            onSubmit={this.handleSubmit}
                            className="layout vertical"
                        >
                            <div className="form-group">     
                                <TextValidator
                                    label="Email"
                                    id="email"
                                    onChange={this.handleChange('email')}
                                    name="email"
                                    value={email}
                                    validators={['required','isEmail']}
                                    errorMessages={['Email has to be filled up','Wrong email address']}
                                    fullWidth
                                    onBlur={this.isDisabled} 
                                />
                                {   
                                    submitted && !email && (
                                        <FormHelperText id="email">Error</FormHelperText>
                                    )
                                } 
                            </div>
                            <div className="form-group">     
                                <FormControl fullWidth error={ submitted && !password }>
                                    <InputLabel htmlFor="password">Password</InputLabel>
                                    <Input
                                        id="password"
                                        type={this.state.showPassword ? 'text' : 'password'}
                                        value={password}
                                        onChange={this.handleChange('password')}
                                        endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="Toggle password visibility"
                                                onClick={this.handleClickShowPassword}
                                                onMouseDown={this.handleMouseDownPassword}
                                                >
                                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                            </IconButton>
                                        </InputAdornment>
                                        }
                                    />
                                    {   
                                        submitted && !password && (
                                            <FormHelperText id="password">Password has to be filled up</FormHelperText>
                                        )
                                    }  
                                </FormControl>
                            </div>
                            <div className="forgot-router">
                                <Link to="/reset-password" className="link">Forgot Password?</Link>
                            </div>
                            <div className="signup-router">
                                Don't have account? 
                                <Link to="/register" className="link"> SIGN UP </Link>
                            </div>  
                            <div className="form-group">
                                <button className="btn-submit layout center-center">
                                    {!loggingIn && <span>SIGN IN</span> }
                                    {loggingIn &&
                                        <img alt="loading gif" src={ButtonLoadingDataImage} />
                                    }
                                </button>
                            </div>
                        </ValidatorForm>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn, error } = state.authentication;
    return {
        loggingIn,
        error
    }
}

const connectedSignInPage = connect(mapStateToProps)(SignInPage);

export default connectedSignInPage;