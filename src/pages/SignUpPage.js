import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import SignBanner from '../components/SignBanner';
import MobileLogo from '../assets/images/logo-black.svg';

import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { ButtonLoadingDataImage } from '../_helpers';

class SignUpPage extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            user: {
                firstName: '',
                lastName: '',
                email: '',
                password: '',
                repeatPassword: ''
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.user.password) {
                return false;
            }
            return true;
        });
    }

    handleChange = (name) => event => {
        const { user } = this.state;

        this.setState({
            user: {
                ...user,
                [name]: event.target.value
            }
        });
        
    };

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if(user.firstName && user.lastName && user.email && user.password) {
            dispatch(userActions.register(user));
        }
    }

    render() { 
        const { registering } = this.props;
        const { user } = this.state;
        const error = '';

        return (
            <div className="sign-container layout horizontal">
                <SignBanner />
                <div className="content flex-8 layout vertical center-center">
                    <Link to="/" className="mobile-logo">
                        <img src={MobileLogo} width="30" height="30" alt="mobile logo" />
                    </Link>
                    
                    <div className="login-card layout vertical register-card">
                        <h1 className="header">Get started absolutely free.</h1>
                        <span className={'notification' + (error ? ' alert' : '')}>
                            { !error ? 'Enter your details below.': error }
                        </span>

                        <ValidatorForm
                            onSubmit={this.handleSubmit}
                            className="layout vertical"
                            >
                            <div className="form-group">     
                                <TextValidator
                                    label="First name"
                                    onChange={this.handleChange('firstName')}
                                    name="firstName"
                                    value={user.firstName}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                    aria-describedby="firstName" 
                                    fullWidth
                                />
                            </div>
                            <div className="form-group">     
                                <TextValidator
                                    label="Last name"
                                    onChange={this.handleChange('lastName')}
                                    name="lastName"
                                    value={user.lastName}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                    aria-describedby="lastName" 
                                    fullWidth
                                />
                            </div>
                            <div className="form-group">     
                                <TextValidator
                                    label="Email"
                                    onChange={this.handleChange('email')}
                                    name="email"
                                    value={user.email}
                                    validators={['required', 'isEmail']}
                                    errorMessages={['this field is required', 'Email is not valid']}
                                    aria-describedby="email" 
                                    fullWidth
                                />
                            </div>
                            <div className="form-group">    
                                <TextValidator
                                    label="Password"
                                    onChange={this.handleChange('password')}
                                    name="password"
                                    type="password"
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                    value={user.password}
                                    aria-describedby="password" 
                                    fullWidth
                                />
                            </div>
                            <div className="form-group">    
                                <TextValidator
                                    label="Repeat password"
                                    onChange={this.handleChange('repeatPassword')}
                                    name="repeatPassword"
                                    type="password"
                                    validators={['isPasswordMatch', 'required']}
                                    errorMessages={['password mismatch', 'this field is required']}
                                    value={user.repeatPassword}
                                    aria-describedby="repeatPassword" 
                                    fullWidth
                                />
                            </div>
                            <div className="signup-router">
                                Already have an account? 
                                <Link to="/login" className="link"> SIGN IN </Link>
                            </div>
                            <div className="form-group" style={{ marginTop: '50px' }}>
                                <button className="btn-submit layout center-center">
                                    {!registering && <span>SIGN UP</span> }
                                    {registering &&
                                        <img alt="loading gif" src={ButtonLoadingDataImage} />  }
                                </button>
                            </div>
                        </ValidatorForm>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    }
}

const connectedSignUpPage = connect(mapStateToProps)(SignUpPage);

export default connectedSignUpPage;